#!/usr/bin/env python
"""
generate a array of 8bit data, convert to manchester code
and sine wavedata
"""


import numpy as np
import matplotlib.pyplot as plt

BITS_NUM = 16
SAMPLES_NUM = 128
MCODE_STYLE = 0
BASE_FEQ = 10000
"""
0->G.E.Tomas code, high->low stand for 1, low->high stand for 0
1010100011101110->10011001100101011010100110101001
1->IEEE 802.3 code, high->low stand for 0, low->high stand for 1
1010100011101110->01100110011010100101011001010110
"""


def main():
    """ entry """
    random_data = np.random.randint(low=0, high=2, size=BITS_NUM)
    print(random_data)
    mcode_binary = []
    nrz_binary = []
    clk_binary = []
    for data in random_data:
        if MCODE_STYLE:
            if data:
                temp = [0, 2]
            else:
                temp = [2, 0]
        else:
            if data:
                temp = [2, 0]
            else:
                temp = [0, 2]
        for i in range(SAMPLES_NUM):
            nrz_binary.append(data)
            if i < SAMPLES_NUM/2:
                clk_binary.append(0)
                mcode_binary.append(temp[0])
            else:
                clk_binary.append(1)
                mcode_binary.append(temp[1])
    sine_wave_0 = np.linspace(0, np.pi, int(SAMPLES_NUM/2))
    sine_wave_1 = np.linspace(np.pi, 2*np.pi, int(SAMPLES_NUM/2))
    sine_wave_2 = np.linspace(0, np.pi/2, int(SAMPLES_NUM/2))
    sine_wave_3 = np.linspace(np.pi/2, np.pi, int(SAMPLES_NUM/2))
    sine_wave_4 = np.linspace(np.pi, 3/2.0*np.pi, int(SAMPLES_NUM/2))
    sine_wave_5 = np.linspace(3/2.0*np.pi, 2*np.pi, int(SAMPLES_NUM/2))

    plt.figure(1)
    time_axis = np.arange(0.0, len(random_data), 1.0/SAMPLES_NUM)
    plt.subplot(411)
    plt.plot(time_axis, clk_binary)
    plt.grid()
    plt.subplot(412)
    plt.plot(time_axis, nrz_binary)
    plt.grid()
    plt.subplot(413)
    plt.plot(time_axis, mcode_binary)
    plt.grid()
    plt.subplot(414)
    mcode_binary_aux = np.array(mcode_binary)*1.0
    print(len(mcode_binary_aux))
    for i in range(0, len(mcode_binary), int(SAMPLES_NUM/2)):
        if i == 0:
            if mcode_binary[i] == 2:
                wave = np.sin(sine_wave_0)
            else:
                wave = np.sin(sine_wave_1)
        elif i < len(mcode_binary) - SAMPLES_NUM/2:
            if mcode_binary[i] > 0:
                if mcode_binary[i] > mcode_binary[i-1] and \
                    mcode_binary[i] == mcode_binary[i+ int(SAMPLES_NUM/2)]:
                    wave = np.sin(sine_wave_2)
                elif mcode_binary[i] > mcode_binary[i-1] and \
                    mcode_binary[i] > mcode_binary[i+ int(SAMPLES_NUM/2)]:
                    wave = np.sin(sine_wave_0)
                else:
                    wave = np.sin(sine_wave_3)
            else:
                if mcode_binary[i] < mcode_binary[i-1] and \
                    mcode_binary[i] < mcode_binary[i+ int(SAMPLES_NUM/2)]:
                    wave = np.sin(sine_wave_1)
                elif mcode_binary[i] < mcode_binary[i-1] and \
                    mcode_binary[i] == mcode_binary[i+1]:
                    wave = np.sin(sine_wave_4)
                else:
                    wave = np.sin(sine_wave_5)
        else :
            if mcode_binary[i] > 0:
                wave = np.sin(sine_wave_0)
            else:
                wave = np.sin(sine_wave_1)
        for j in range(int(SAMPLES_NUM/2)):
            mcode_binary_aux[i+j] = wave[j]
        
    plt.plot(time_axis, mcode_binary_aux)
    plt.grid()

    plt.figure(2)
    plt.subplot(321)
    plt.plot(sine_wave_0, np.sin(sine_wave_0))
    plt.grid()
    plt.subplot(322)
    plt.plot(sine_wave_1, np.sin(sine_wave_1))
    plt.grid()
    plt.subplot(323)
    plt.plot(sine_wave_2, np.sin(sine_wave_2))
    plt.grid()
    plt.subplot(324)
    plt.plot(sine_wave_3, np.sin(sine_wave_3))
    plt.grid()
    plt.subplot(325)
    plt.plot(sine_wave_4, np.sin(sine_wave_4))
    plt.grid()
    plt.subplot(326)
    plt.plot(sine_wave_5, np.sin(sine_wave_5))
    plt.grid()
    plt.show()
    

if __name__ == "__main__":
    main()
